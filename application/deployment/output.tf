output "address" {
  value = "${aws_elb.main.dns_name}"
}
