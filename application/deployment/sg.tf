resource "aws_security_group" "app" {
  name = "${var.name}-app-sg"
  description = "Security group for ${var.name} Application"
  vpc_id = "${data.terraform_remote_state.infrastructure.vpc_id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    security_groups = ["${aws_security_group.elb.id}"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.name}-app-sg"
    Application = "${var.name}"
  }
}

resource "aws_security_group" "elb" {
  name = "${var.name}-elb-sg"
  description = "Security group for ELB that allows web traffic from internet"
  vpc_id = "${data.terraform_remote_state.infrastructure.vpc_id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.name}-elb-sg"
    Application = "${var.name}"
  }
}
