resource "aws_autoscaling_group" "main" {
  availability_zones = ["${var.zones}"]
  name = "${var.name}-${aws_launch_configuration.main.name}"
  max_size = 5
  min_size = 1
  wait_for_elb_capacity = 2
  desired_capacity = 2
  health_check_grace_period = 300
  health_check_type = "ELB"
  launch_configuration = "${aws_launch_configuration.main.id}"
  load_balancers = ["${aws_elb.main.id}"]
  vpc_zone_identifier = ["${data.terraform_remote_state.infrastructure.private_subnets}"]

  tag {
    key = "Name"
    value = "${var.name}-app"
    propagate_at_launch = true
  }

  lifecycle { 
    create_before_destroy = true
  }
}
