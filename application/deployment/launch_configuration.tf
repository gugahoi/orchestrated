resource "aws_launch_configuration" "main" {
  image_id = "${lookup(var.ami, var.region)}"
  instance_type = "t2.medium"
  key_name = "${var.ssh_key}"
  # associate_public_ip_address = "true"

  security_groups = ["${aws_security_group.app.id}"]
  user_data = "${data.template_file.user_data.rendered}"

  lifecycle { 
    create_before_destroy = true 
  }
}

data "template_file" "user_data" {
   template = "${file("${path.module}/user_data.tpl")}"

   vars {
     IMAGE = "${var.application_image}"
     TAG = "${var.application_tag}"
     CONTAINER_PORT = "${var.application_port}"
   }
}
