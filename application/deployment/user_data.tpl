#!/usr/bin/env bash

echo "--- Running Application"
HOST_PORT=80
docker run --restart always -p $${HOST_PORT}:${CONTAINER_PORT} ${IMAGE}:${TAG}
