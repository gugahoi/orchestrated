variable "region" { 
  type = "string"
  default = "ap-southeast-2"
}

variable "zones" {
  type = "list"
  default = ["a","b","c"]
  description = "Which Availability Zones to use. These values will be appended to the region, for example when zone = ['a','b'] and region='some-region' the computed AZ will be 'some-regiona' and 'some-regionb'" 
}

variable "name" {
  description = "Name of the application to launch"
  type = "string"
}

variable "ssh_key" {
  description = "Key Pair name to SSH into into instances"
  type = "string"
  default = ""
}

variable "application_image" {
  description = "URL to application's docker image"
  type = "string"
  default = "nginx"
}

variable "application_tag" {
  description = "Application docker image tag"
  type = "string"
  default = "1.13-alpine"
}

variable "application_port" {
  description = "Application container port"
  type = "string"
  default = "80"
}

variable "ami" {
  type = "map"
  description = "AMIs to run. Preferably ones that come with docker pre installed"
  default = {
    us-east-2 = "ami-1c002379"
    us-east-1 = "ami-9eb4b1e5"
    us-west-2 = "ami-1d668865"
    us-west-1 = "ami-4a2c192a"
    eu-west-2 = "ami-cb1101af"
    eu-west-1 = "ami-8fcc32f6"
    eu-central-1 = "ami-0460cb6b"
    ap-northeast-1 = "ami-b743bed1"
    ap-southeast-2 = "ami-c1a6bda2"
    ap-southeast-1 = "ami-9d1f7efe"
    ca-central-1 = "ami-b677c9d2"
  }
}
