resource "aws_elb" "main" {
  name = "${var.name}-elb"

  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 2
    timeout = 10
    target = "HTTP:80/"
    interval = 30
  }

  cross_zone_load_balancing = true
  idle_timeout = 10
  subnets         = ["${data.terraform_remote_state.infrastructure.public_subnets}"]
  security_groups = ["${aws_security_group.elb.id}"]

  tags {
    Name = "${var.name}-elb"
  }
}
