TAG ?= latest
assets/%: IMAGE = gugahoi/orchestrated-assets
java/%: IMAGE = gugahoi/orchestrated-java

java/build assets/build: %/build:
	docker build -t ${IMAGE}:${TAG} application/$*

java/publish assets/publish: %/publish: %/build
	docker push ${IMAGE}:${TAG}

java/plan assets/plan: %/plan:
	docker-compose run --rm deployment plan -var name=$* -state $*.tfstate -var-file=$*.tfvar

java/apply assets/apply: %/apply:
	docker-compose run --rm deployment apply -var name=$* -state $*.tfstate -var-file=$*.tfvar

java/destroy assets/destroy: %/destroy:
	docker-compose run --rm deployment destroy -var name=$* -state $*.tfstate -var-file=$*.tfvar

run:
	docker-compose up --build -d assets java

stop:
	docker-compose down

init:
	docker-compose run --rm infrastructure init
	docker-compose run --rm deployment init

infra/init:
	docker-compose run --rm infrastructure init
infra/plan:
	docker-compose run --rm infrastructure plan
infra/apply:
	docker-compose run --rm infrastructure apply
infra/destroy:
	docker-compose run --rm infrastructure destroy

.PHONY: run stop infra/init infra/plan infra/apply infra/destroy java/build assets/build java/publish assets/publish java/plan assets/plan java/apply assets/apply java/destroy assets/destroy
