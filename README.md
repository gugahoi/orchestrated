# CI/CD

## Table of Contents

1. [Demo](#demo)
1. [Infrastructure](#infrastructure)
1. [Applications](#applications)
1. [Principles](#principles)

## Demo

[![asciicast](https://asciinema.org/a/acdsPEmC7MqM5IJWQM7ubyFLc.png)](https://asciinema.org/a/acdsPEmC7MqM5IJWQM7ubyFLc)

## Infrastructure


### Description

This project will create a VPC with:

- 3 public subnets
  - accross 3 availability zones for HA
  - attached to an Internet Gateway
- 3 private subnets
  - accross 3 availavbility zones for HA
  - attached to NAT Gateways

This is a simplistic network to host applications where Load Balancer's can be set up in public subnets and route traffic to applications hosted in the private subnets. It could be further expanded to included DB exclusive subnets, where the traffic is limited to come from the private subnets only.

![infrastructure diagram](./images/infrastructure-diagram.png)

### Setting it up

> Make sure to configure AWS credentials appropriately in `~/.aws/` folder as per [Amazon's Documentation](http://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html)

```bash
# initialise terraform
make infra/init

# check what is going to be created in AWS
make infra/plan

# create infrastructure resources (vpc, subnet, igw, nats, ...)
make infra/apply

... USE THE ENVIRONMENT ...

# destroy (if needed to clean up)
make infra/destroy
```

### Enhancements

For this example, a local terraform state is used, however when working in teams, remote state should be considered so that any developer as well as CI has access to the latest infrastructure state.

### Multiple Environments

As everything is scripted, all the way from infrasctructure configuration to building and deploying the applications, it would be very straight forward to bring up as many environments as possible when needed, with minor modifications. Furthermore as there is an expectation that the application will be running in containers, we can easily also test them locally in somewhat similar conditions to how they would be running in the cloud.

## Applications

### Development Considerations

Applications should follow [12 factor](https://12factor.net/) principles.

### Assets Application

A hello world html file was used to demonstrate the web server funcionality. This application is built with a default NGINX docker image and served on port 80. Locally, docker-compose runs the application on host port 8081.

The application can be found under `./application/assets`. To run it locally you can simply use `docker-compose up assets` or use one of the provided make tasks.

The docker image created for this application can be found here: https://hub.docker.com/r/gugahoi/orchestrated-assets/

### Java Application

[TOMCAT's sample WAR application](https://tomcat.apache.org/tomcat-8.0-doc/appdev/sample/) was used for the purpose of this excercise, it can easily be replaced with any WAR file that is compatible with TOMCAT. This application is built with the standard TOMCAT docker image and served over port 8080 locally which is the default configuration for TOMCAT.

The application can be found under `./application/java`. To run it locally you can simply use `docker-compose up java` or use one of the provided make tasks.

The docker image created for this application can be found here: https://hub.docker.com/r/gugahoi/orchestrated-java/

### Makefile

${APP} can be either `java` or `assets`

| Task           | Description                                    |
| -------------- | ---------------------------------------------- |
| ${APP}/build   | builds the docker image for ${APP}             |
| ${APP}/publish | publishes the docker image for ${APP}          |
| ${APP}/plan    | shows any AWS changes that a deploy would cause|
| ${APP}/apply   | deploys the application                        |
| ${APP}/destroy | deletes the deployment                         |
| init           | initialize terraform's dependencies            |
| run            | runs boths applications with docker compose up |
| stop           | stops running both applications                |
| infra/init     | initialize terraform's dependencies            |
| infra/plan     | shows the changes of the underlying infrastructure  |
| infra/apply    | applies changes to the underlying infrastructure    |
| infra/destroy  | deletes the underlying infrastrucure                |

### Docker Compose

```bash
# serve java and assets applications in background
docker-compose up -d java assets # or simply use `make run`

# what is running?
docker-compose ps

# assets are served on port 8081
curl http://localhost:8081/

# java is served on port 8080
curl http://localhost:8080/
# the sample war file can be found here
curl http://localhost:8080/sample/

# stop serving applications
docker-compose down # or simply use `make stop`
```

### Deploying the Apps

```
# init terraform dependencies
make init

# check out if any changes need to be applied
make assets/plan

# deploy
make assets/apply

# PROFIT!
```

## Pipeline

The creation of a CI/CD pipeline is pretty straight forward now that most of the tasks are nicely encapsulated. A sample pipeline could be:

```bash
#!/usr/bin/env bash

# lets build the assets!
# first we need to get the provided zip file
curl -O http://path/to/assets.zip

# unzip it
unzip assets.zip -d application/assets/src

# publish a new version of our application
TAG=New-Version make assets/publish

# lets deploy it to our env
make assets/apply

# if this succeded, the new version is well and working otherwise the old version remained and some debugging needs to be done.
```

The above example is a simplistic approach, more monitoring and complexity can be added such as tracking latency and response time to ensure new deployments meet certain standards.

## Public Rollout

To cater for the rollout of the application a simple and effective approach would be to gradually migrate traffic over to the application's elbs. This can be achieve with weighted Route53 records where the percent of traffic can be gradually increased. Another thing that can be taken into account is pre warming ELBs if there is expected to be a spike in traffic along with incresing the disered capacity of the Auto Scalling Group so more instances will be able to handle more load.

## Principles

There are some basic principles that can easily be seen in this project

- Immutable Infrastructure: This deployment strategy always uses new ASGs and creates new instances instead of modifying running instances.
- Zero Downtime Deployment: With the aido of terraform a rolling deployment is done is a manner such that old instances are only taken out of order once the new instances are up and healthy
- 12 Factor Apps: applications are deplyed in docker containers and expect to follow best practices such as Evironment Variables for configuration, logging to `stdout`, no including secrets in source code and so forth.
