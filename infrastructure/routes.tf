resource "aws_route_table" "public" {
  vpc_id           = "${aws_vpc.main.id}"
  tags = {
    Name = "public-all"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table" "private" {
  count  = "${length(var.zones)}"
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name = "private-${element(var.zones, count.index)}"
  }
}

resource "aws_route_table_association" "private" {
  count = "${length(var.zones)}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

resource "aws_route_table_association" "public" {
  count = "${length(var.zones)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
