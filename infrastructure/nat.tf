resource "aws_eip" "nat" {
  count = "${length(var.zones)}"
  vpc = true
}

resource "aws_nat_gateway" "main" {
  count = "${length(var.zones)}"
  allocation_id = "${element(aws_eip.nat.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"

  depends_on = ["aws_internet_gateway.main"]
}

resource "aws_route" "private_nat_gateway" {
  count = "${length(var.zones)}"

  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.main.*.id, count.index)}"
}
