provider "aws" {
  region = "${var.region}"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "Orchestrated"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_subnet" "public" {
  count = "${length(var.zones)}"
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.${count.index}.0/24"
  availability_zone = "${var.region}${element(var.zones, count.index)}"

  tags {
    Name = "public-${element(var.zones, count.index)}"
    Type = "public"
  }
}

resource "aws_subnet" "private" {
  count = "${length(var.zones)}"
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.${count.index + 10}.0/24"
  availability_zone = "${var.region}${element(var.zones, count.index)}"

  tags {
    Name = "private-${element(var.zones, count.index)}"
    Type = "private"
  }
}
