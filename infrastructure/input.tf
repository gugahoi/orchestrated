variable "region" { 
  type = "string"
  default = "ap-southeast-2"
}

# these represent the AZ's, for example (ap-souteast-2a, ap-souteast-2b and ap-souteast-2c)
variable "zones" {
  type = "list"
  default = ["a","b","c"]
  description = "Which Availability Zones to use. These values will be appended to the region, for example when zone = ['a','b'] and region='some-region' the computed AZ will be 'some-regiona' and 'some-regionb'" 
}
